# Sudoku

## Correr

```
pnpm run dev
```

Al usar vscode o vscodium se recomienda tener instalada la extensión volar, y desactivar el plugin builtin de typescript para el workspace.


## Proceso

### 1 - creación del proyecto

Usamos [Vite](https://vitejs.dev/guide/) para crear el proyecto usando Vue + TypeScript.

```
pnpm create vite
```

Eliminamos código de ejemplo (`components/HelloWorld`, assets y html innecesario)

### 2 - agregamos vitest

```
pnpm add -D vitest
```

Agregamos los scripts `test` y `coverage` al `package.json`

Creamos los archivos `sudoku.ts` y `sudoku.test.ts` con la primera prueba y su implementación (`isCorrect`).

Agregamos `coverage` a `.gitignore`

### 3 - dibujamos el sudoku

Creamos una función básica para devolver una matriz de 9x9 vacía.

Agregamos en `App.vue` el primer dibujo del sudoku usando divs y css.


### 4 - agregamos movimientos

Permitimos mover la selección mediante teclado y mouse, y además permitimos ingresar un número.


### 5 - agregamos celdas fijas

Cambiamos la representación del sudoku desde una matriz de numbers, a una matriz de objects.

Creamos un sudoku de ejemplo con valores iniciales que deberían permanecer fijos (`frozen`) y le dimos estilo.

### 6 - agregamos highlights y condición de victoria

Agregamos resaltado para valor seleccionado.

Agregamos función que devuelve a partir de un sudoku, un sudoku con los valores erróneos marcados como `repeated` y en base a eso le agregamos un resaltado (verificamos por row, col, y subgrid). 

Agregamos una condición de victoria usando la función anterior, verificando que ningún valor sea repetido, ni esté vacío.

### 7 - sudoku "responsivo", state a pinia, agregado de controles, composables para hotkeys

#### sudoku "responsivo"

En lugar de setear en cada `.col` un width fijo de `2em`, se cambiaron `.sudoku`, `.row` y `.col` a que sean `display: flex`. Se setea el `aspect-ratio: 1/1` del `.sudoku`.
Se crea el componente `Sudoku` y se mueve allí gran parte de la funcionalidad que estaba en `App`.

#### state a pinia

Se agrega [pinia](https://pinia.vuejs.org/) como dependencia, se agrega su setup en `main.ts`, y se crea `stores/game.ts`, a donde se mueve el estado y se crean acciones con la lógica que estaba dispersa en el componente `App`/`Sudoku`.

#### agregado de controles

Se crea `Controls.vue` que interactúa con la funcionalidad del store, como alternativa de input a los eventos del teclado, especialmente para dispositivos móbiles.

#### composables para hotkeys

Se agrega la dependencia [@vueuse/core](https://vueuse.org/). Se crea `composables/useKeyboard.ts`, lo que sería un composable/hook custom para bindear atajos de teclado a funcionalidad en el store. Para esto se usa el composable [`onKeyStroke`](https://vueuse.org/core/onKeyStroke/). 

### 8 - Highlight de areas relacionadas a la seleccion

Se marcan la row, columna, y subgrilla de la celda seleccionada. Para esto se agrega al store el getter `selectedAreas`.

### 9 - Agregamos timer y Menu

Mostramos un timer en `Controls.vue`. 
Agregamos la lógica al store `game.ts`: se setea `startedAt` al iniciar un sudoku y luego se calcula el tiempo que pasó contra eso. 
Se agrega un `Menu.vue` básico para poder dar inicio a un sudoku. Se agregan las actions `start` y `stop` al store.

### 10 - Agregamos anotaciones

Agregamos la propiedad `candidates` a la celda del sudoku, como un array de `SudokuValues`. Agregamos `toggleCandidate` que agrega o quita un elemento de esa propiedad si ya existe.
Creamos el Componente `CandidatesGrid.vue` para mostrar las anotaciones activas.
Agregamos en `useKeyboard` la posibilidad de agregar candidatos manteniendo presionado `Ctrl`.
Agregamos el toggle de modo de anotaciones usando `Controls.vue`.

### 11 - Agregamos themes

Agregamos el store `theme.ts`, el componente `ThemeSwitcher.vue` y aplicamos los estilos utilizando [`v-bind`](https://vuejs.org/api/sfc-css-features.html#v-bind-in-css) y variables css.
Como vimos que [no se puede](https://github.com/vuejs/core/issues/5007) aplicar v-bind en el body usando styles que no sean scoped, seteamos directamente `document.body.style` en ese caso.

### 12 - Agregamos generacion 

Se agrego la generacion automatica en `game.ts` usando [`@algorithm.ts/sudoku`](https://www.npmjs.com/package/@algorithm.ts/sudoku).
También se marcan las keys numéricas que ya están completadas.

### 13 - Seleccion de dificultad

Se agrego el algoritmo para generar sudoku segun dificultad y su seleccion.

### 14 - Fix del timer

Se fixea el timer, se utiliza el hook `useStopwatch` de la lib `vue-timer-hook`.

### 15 - Deshacer/rehacer + reset

Se mueve el store `game.ts` de [option store](https://pinia.vuejs.org/core-concepts/#Option-Stores) a [setup store](https://pinia.vuejs.org/core-concepts/#Setup-Stores). De esta manera se puede usar el hook [useManualRefHistory](https://vueuse.org/core/useManualRefHistory/) para mantener el historial del sudoku y facilitar la tarea de deshacer y rehacer.
Se implementa `clear` y el reset del estado del juego a 0.

### 16 - Limpiar candidatos relacionados a la jugada

Se agrega la función `clearCandidates` a `sudoku.ts` con el fin de limpiar los candidatos de las celdas relacionadas. Por ejemplo, si juego un 1, todos los candidatos 1 que estén en la misma row, columna y subgrilla se eliminan.

## TODO

- Agregar atajo para movimiento salteando frozen cells (ctrl+dir?)
- Crear sudoku custom y compartir via url
    - Validarlo / asegurarse de que tenga una única solución
- Sudokus precargados?
- Competir en vivo con webrtc / de forma descentralizada
- Persistir sesión de juego mas allá del cierre del navegador (pausar y reanudar timer, en lugar de comparar contra fecha fija de inicio)
- theme
    - personalizar?
    - useDark? setar por defecto un theme claro o oscuro segun setting del SO