import { onKeyStroke } from "@vueuse/core";
import { useGameStore } from "../stores/game";

export default function useKeyboard() {
  const store = useGameStore()

  onKeyStroke(["w", "W", "ArrowUp"], () => {
    store.move('ArrowUp')
  });

  onKeyStroke(["s", "S", "ArrowDown"], () => {
    store.move('ArrowDown')
  });

  onKeyStroke(["a", "A", "ArrowLeft"], () => {
    store.move('ArrowLeft')
  });

  onKeyStroke(["d", "D", "ArrowRight"], () => {
    store.move('ArrowRight')
  });
  
  onKeyStroke(["z", "Z"], (e) => {
    if (e.ctrlKey && store.hasUndo.value){
      store.undo();
    }
  });
  
  onKeyStroke(["y", "Y"], (e) => {
    if (e.ctrlKey && store.hasRedo.value){
      store.redo();
    }
  });
  
  onKeyStroke(["Escape"], () => {
    store.setSelected(null)
  });

  onKeyStroke(["Delete", "Backspace"], () => {
    if (store.selected){
        store.play('0')
    }
  })

  onKeyStroke(['Control'], () => {
    store.toggleAnnotationEnabled();
  })

  onKeyStroke([' ', 'x'], () => {
    if (store.currentNumber){
      const erase: boolean = store.currentNumber === store.selectedCell?.value;
      if(store.annotationEnabled){
            store.playCandidate(erase ? '0' : store.currentNumber + '');
        } else {
          store.play(erase ? '0' : store.currentNumber + '');
        }
    }
  })

  onKeyStroke((e) => {
    if (e.key.match(/^[1-9]$/)){
      store.setCurrentNumber(e.key);
    }
  })
}
