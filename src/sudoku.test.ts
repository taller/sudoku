import { expect, test, describe } from 'vitest'
import { isGroupCorrect, init, isSolved, play, cell, cells } from './sudoku'
import type { Sudoku } from './sudoku'

describe('isGroupCorrect', () => {
    test('retorna false si no hay 9 elementos en el array', () => {
        expect(isGroupCorrect([])).toBe(false)
        expect(isGroupCorrect([cell(1)])).toBe(false)
        expect(isGroupCorrect(cells([1,2,3,4,5,6,7,8]))).toBe(false)
    });
    test('retorna false si hay al menos un elemento vacío (0)', () => {
        expect(isGroupCorrect(cells([0,2,3,0,5,6,7,8,0]))).toBe(false)
        expect(isGroupCorrect(cells([1,2,3,4,5,6,7,8,0]))).toBe(false)
    })
    test('retorna true si todos los elementos son diferentes', () => {
        expect(isGroupCorrect(cells([1,2,3,4,5,6,7,8,9]))).toBe(true)
        expect(isGroupCorrect(cells([9,2,8,4,5,6,7,3,1]))).toBe(true)
    })
    test('retorna false si hay algun elemento repetido', () => {
        expect(isGroupCorrect(cells([9,9,9,4,5,6,7,8,9]))).toBe(false)
        expect(isGroupCorrect(cells([9,2,3,4,5,6,7,8,9]))).toBe(false)
        expect(isGroupCorrect(cells([1,2,3,4,2,6,7,8,9]))).toBe(false)
        expect(isGroupCorrect(cells([1,2,3,4,5,6,7,8,1]))).toBe(false)
    })
});

describe('init', () => {
    test('devuelve una matriz de 9x9', () => {
        const result = init(0.2)
        expect(result).toHaveLength(9)
        result.forEach(row => {
            expect(row).toHaveLength(9)
        })
    });
})

describe('isSolved', () => {
    test('devuelve false si el sudoku está incompleto', () => {
        expect(isSolved([])).toBe(false)
    });
});

describe('play', () => {
    const empty: Sudoku = [
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0]),
        cells([0, 0, 0, 0, 0, 0, 0, 0, 0])
      ]
    test('devuelve un nuevo sudoku con el elemento provisto en las coordenadas', () => {
        const result = play(empty, 1, [0,0]);
        expect(result[0][0].value).toEqual(1)
    });
});