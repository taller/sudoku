import { SudokuCreator } from '@algorithm.ts/sudoku'


export const SUDOKU_COORDS = [0, 1, 2, 3, 4, 5, 6, 7, 8] as const;
export const SUDOKU_NUMBERS = [1, 2, 3, 4, 5, 6, 7, 8, 9] as const;
export type SudokuCoord = typeof SUDOKU_COORDS[number]
export type SudokuNumber = typeof SUDOKU_NUMBERS[number]
export type Empty = 0
export type SudokuValue = SudokuNumber | Empty
export type SudokuCell = {
    value: SudokuValue;
    frozen: boolean;
    repeated?: boolean;
    candidates?: SudokuNumber[]
}

export type SudokuGroup = SudokuCell[]
export type Sudoku = SudokuCell[][]

export type SudokuGame = {
    difficulty: number;
    puzzle: number[];
}

export type SudokuGameFinished = SudokuGame & {
    time: string;
}

export function cell(value: SudokuValue, frozen = false): SudokuCell {
    return { value, frozen };
}

export function cells(values: SudokuValue[], options: Partial<SudokuCell> = {}): SudokuCell[] {
    return values.map(value => ({ value, frozen: false, ...options }))
}

/**
 * Retorna verdadero si no hay numeros repetidos y si hay exactamente 9 números no vacíos
 * @param array 
 * @returns 
 */
export function isGroupCorrect(group: SudokuGroup): boolean {
    const array = group.map(x => x.value)
    if (array.length !== 9 || array.some(n => n === 0)){
        return false;
    }
    return [...new Set(array)].length === 9;
}

export function fillSudoku(base: Sudoku, fills: Sudoku): Sudoku {
    return base.map((row, rowIndex) => {
        return row.map((cell, colIndex) => cell.frozen ? cell : fills[rowIndex][colIndex])
    })
}

export function toGrid(puzzle: number[]): Sudoku {
    const chunkSize = 9;
    const all = [];
    for (let i = 0; i < puzzle.length; i += chunkSize) {
        all.push(cells(puzzle.slice(i, i + chunkSize) as SudokuValue[]));
    }
    return all.map(row => row.map(cell => ({...cell, frozen: cell.value !== 0 })));
}

export function toArray(sudoku: Sudoku): number[] {
    return sudoku.flat().map(x => x.value);
}

export function init(difficulty: number): Sudoku {
    const creator = new SudokuCreator({ childMatrixWidth: 3})
    const sudoku = creator.createSudoku(difficulty);
    const puzzle = sudoku.puzzle.map(x => x + 1);
    return toGrid(puzzle);
}

export function clear(sudoku: Sudoku): Sudoku {
    //TODO: how to implement in other way? is correct to mutate the cells?
    for (let rowIndex = 0; rowIndex < sudoku.length; rowIndex++) {
        const row = sudoku[rowIndex];
        for (let colIndex = 0; colIndex < row.length; colIndex++) {
            const element = row[colIndex];
            if (!element.frozen){
                element.value = 0;
                element.candidates = undefined;
            }
            element.repeated = undefined;
        }
        
    }
    // this wont work:
    // return sudoku.map(row => row.map(c => c.frozen ? {...c} : cell(0)));
    return sudoku;

}

export function initEmpty(): Sudoku {
    return [...Array.from({ length: 9 })].map(_ => [...Array.from({ length: 9 })].map(__ => cell(0, true)));
}

export function markRepeatedSubGrids(input: Sudoku): Sudoku {
    const sudoku = input.map(row => row.map(x => ({...x})));
    for (let startRow = 0; startRow < 9; startRow += 3) {
        for (let startCol = 0; startCol < 9; startCol += 3) {
            const subgridSet = new Set<number>();
            const subgridRepeated = new Set<number>();
            for (let row = startRow; row < startRow + 3; row++) {
                for (let col = startCol; col < startCol + 3; col++) {
                    const num = sudoku[row][col].value;
                    if (num === 0) {
                        continue;
                    }
                    if (subgridSet.has(num)) {
                        subgridRepeated.add(num);
                    } else {
                        subgridSet.add(num);
                    }
                }
            }
            for (let row = startRow; row < startRow + 3; row++) {
                for (let col = startCol; col < startCol + 3; col++) {
                    sudoku[row][col] = { ...sudoku[row][col], repeated: sudoku[row][col].repeated ? true : subgridRepeated.has(sudoku[row][col].value) };
                }
            }
        }
    }
    return sudoku;
}

export function markRepeatedRows(sudoku: Sudoku): Sudoku {
    return sudoku.map(row => {
        const wrongNumbersInRow = row.map(x => x.value)
            .filter(x => x !== 0)
            .filter(x => row.filter(r => r.value === x).length > 1)
        
        return row.map(cell => {
            return {...cell, repeated: wrongNumbersInRow.includes(cell.value) }
        })
    })
}

export function markRepeatedCols(sudoku: Sudoku): Sudoku {
    const numbersRepeatedInColumns: Array<Set<SudokuValue>> = Array.from({ length: 9 }, () => new Set<SudokuValue>())
    for (let col = 0; col < 9; col++) {
        const numbersInColumn = new Set()
        for (let row = 0; row < 9; row++) {
            const number = sudoku[row][col].value;
            if (number === 0){
                continue;
            }
            if (numbersInColumn.has(number)){
                numbersRepeatedInColumns[col].add(number)
            } else {
                numbersInColumn.add(number)
            }
        }
    }
    return sudoku.map(row => {
        return row.map((cell, colIndex) => {
            return {...cell, repeated: cell.repeated ? cell.repeated : numbersRepeatedInColumns[colIndex].has(cell.value) }
        })
    })
}


export function markRepeated(sudoku: Sudoku): Sudoku {
    return markRepeatedSubGrids(markRepeatedCols(markRepeatedRows(sudoku)))
}

export function clearCandidate(sudoku: Sudoku, num: SudokuValue, position: [number, number]): Sudoku {
    const subgridIndex = (pos: [number, number]): [number, number] => 
        [Math.floor(pos[0] / 3), Math.floor(pos[1] / 3)]
    const [subgridRow, subgridCol] = subgridIndex(position);
    return sudoku.map((row, i) => {
        return row.map((cell, j) => {
            if ((i === position[0] || j === position[1]) && cell.candidates){
                return { ...cell, candidates: cell.candidates.filter(x => x !== num) };
            }
            const [currentSubgridRow, currentSubgridColumn] = subgridIndex([i, j]);
            if (subgridRow === currentSubgridRow && subgridCol === currentSubgridColumn && cell.candidates){
                return { ...cell, candidates: cell.candidates.filter(x => x !== num) };
            }
            return cell;
        });
    });
}

export function play(sudoku: Sudoku, num: SudokuValue, position: [number, number]): Sudoku {
    // console.log(`Played ${num} on position [${position[0]}, ${position[1]}]`)
    const sudokuAfterPlay = sudoku.map((row, i) => {
        if (i === position[0]){
            return row.map((cell, j) => {
                if (j === position[1]){
                    return { ...cell, value: num, candidates: undefined };
                }
                return cell;
            })
        } else {
            return [...row];
        }
    })
    const sudokuWithRepeated = markRepeated(sudokuAfterPlay);
    return clearCandidate(sudokuWithRepeated, num, position);
}

export function removeCandidate(sudoku: Sudoku, num: SudokuValue, selectedAreas: boolean[][]): Sudoku {
    return sudoku.map((row, i) => {
      return row.map((col, j) => {
        if (selectedAreas[i][j]){
            return { ...col, candidates: col.candidates?.filter(n => n !== num) };
        } else {
            return col;
        }
      })  
    });
}

export function toggleCandidate(sudoku: Sudoku, num: SudokuNumber, position: [number, number]): Sudoku {
    // console.log(`toggleCandidate ${num} on position [${position[0]}, ${position[1]}]`)
    const sudokuAfterPlay = sudoku.map((row, i) => {
        if (i === position[0]){
            return row.map((cell, j) => {
                if (j === position[1]){
                    const newCandidates = [...(cell.candidates || [])];
                    return { 
                        ...cell, 
                        candidates: newCandidates.includes(num) ? 
                            newCandidates.filter(n => n !== num) : 
                            [...newCandidates, num] };
                }
                return cell;
            })
        } else {
            return [...row];
        }
    })
    return sudokuAfterPlay;
}

export function isSolved(sudoku: Sudoku): boolean {
    return sudoku.flat().every(cell => cell.value !== 0 && !cell.repeated)
}
