import { defineStore } from "pinia";
import {
  Sudoku,
  SudokuCoord,
  init,
  SUDOKU_COORDS,
  SUDOKU_NUMBERS,
  initEmpty,
  toggleCandidate,
  play as sudokuPlay,
  isSolved as sudokuIsSolved,
  clear,
  SudokuGameFinished,
  toGrid,
  SudokuValue,
} from "../sudoku";
import { includes } from "../typeutils";
import { ResUseStopwatch, useStopwatch } from "vue-timer-hook";
import { useManualRefHistory } from '@vueuse/core'
import { computed, ref } from "vue";
import { useMetaStore } from "./meta";
export type Coords = [SudokuCoord, SudokuCoord];

export type GameState = {
  sudoku: Sudoku;
  selected: Coords | null;
  startedAt: Date | null;
  annotationEnabled: boolean;
  stopwatch: ResUseStopwatch;
};

function getSubgrid([row, col]: [SudokuCoord, SudokuCoord]): [number, number] {
  return [Math.floor(row / 3), Math.floor(col / 3)];
}

export const useGameStore = defineStore("game", () => {
  const sudoku = ref<Sudoku>(initEmpty());
  const selected = ref<Coords | null>(null);
  const startedAt = ref<Date | null>(null);
  const annotationEnabled = ref<boolean>(false);
  const stopwatch = useStopwatch(0, false);
  const currentNumber = ref<SudokuValue>(0);
  
  const setCurrentNumber = function (value: string) {
    const n = parseInt(value)
    if (!includes(SUDOKU_NUMBERS, n)) {
      throw new Error(`Invalid sudoku value: ${value}`);
    }
    if (n === currentNumber.value){
      currentNumber.value = 0;
    } else {
      currentNumber.value = n;
    }
  }

  let history = useManualRefHistory(sudoku)
  const meta = useMetaStore()

  const started = computed(() => {
    return !!startedAt.value;
  });

  const selectedCell = computed(() => {
    if (!selected.value) {
      return null;
    }
    return sudoku.value[selected.value[0]][selected.value[1]];
  });

  const isSolved = computed(() => {
    return sudokuIsSolved(sudoku.value);
  });

  const hasUndo = computed(() => history.canUndo);
  const hasRedo = computed(() => history.canRedo);

  const numbersEnabled = computed(() => {
    return selectedCell.value ? !selectedCell.value.frozen : false;
  });

  const selectedAreas = computed<boolean[][]>(() => {
    return sudoku.value.map((row, rowIndex) =>
      row.map((_col, colIndex) => {
        if (!selected.value) {
          return false;
        } else if (rowIndex === selected.value[0]) {
          return true;
        } else if (colIndex === selected.value[1]) {
          return true;
        } else {
          const selectedSubgrid = getSubgrid(selected.value);
          const currentSubgrid = getSubgrid([
            rowIndex as SudokuCoord,
            colIndex as SudokuCoord,
          ]);
          return (
            selectedSubgrid[0] === currentSubgrid[0] &&
            selectedSubgrid[1] === currentSubgrid[1]
          );
        }

      })
    );
  });
  const elapsedTime = computed(() => {
    return `${stopwatch.minutes.value.toString().padStart(2, "0")}:${stopwatch.seconds.value
      .toString()
      .padStart(2, "0")}`;
  });
  const valuesCompleted = computed<boolean[]>(() => {
    const result = [true];
    for (let i = 1; i <= 9; i++) {
      result.push(
        sudoku.value
          .flat()
          .map((x) => x.value)
          .filter((x) => x === i).length === 9
      );
    }
    return result;
  });

  function start(difficulty: number) {
    sudoku.value = init(difficulty);
    history.clear();
    history = useManualRefHistory(sudoku)
    startedAt.value = new Date();
    stopwatch.start();
    meta.start(sudoku.value, difficulty);
  }

  function startChallenge(challenge: SudokuGameFinished){
    window.history.pushState({}, document.title, window.location.pathname);
    sudoku.value = toGrid(challenge.puzzle)
    history.clear();
    history = useManualRefHistory(sudoku)
    startedAt.value = new Date();
    stopwatch.start();
    meta.start(sudoku.value, challenge.difficulty);
  }

  function stop() {
    startedAt.value = null;
    stopwatch.reset();
  }

  function restart() {
    sudoku.value = clear(sudoku.value);
    startedAt.value = new Date();
    stopwatch.reset();
    history.clear();
    setSelected(null);
  }

  function setSelected(coords: [number, number] | null) {
    if (!coords) {
      selected.value = null;
    } else if (
      !includes(SUDOKU_COORDS, coords[0]) ||
      !includes(SUDOKU_COORDS, coords[1])
    ) {
      throw new Error(`Incorrect coordinates: ${coords}`);
    } else {
      selected.value = [coords[0], coords[1]];
    }
  }
  function move(key: string) {
    // TODO: narrow type key?
    const [row, col] = selected.value || [0, 0];
    if (key === "ArrowRight") {
      setSelected([row ?? 0, (col + 1) % 9]);
    } else if (key === "ArrowLeft") {
      setSelected([row, col === 0 ? 8 : col - 1]);
    } else if (key === "ArrowUp") {
      setSelected([row === 0 ? 8 : row - 1, col]);
    } else if (key === "ArrowDown") {
      setSelected([(row + 1) % 9, col]);
    }
  }
  function playNumber(number: number) {
    if (!includes(SUDOKU_NUMBERS, number) && number !== 0) {
      throw new Error(`Invalid sudoku value: ${number}`);
    }
    if (!selected.value) {
      throw new Error("Cannot play without selection");
    }
    sudoku.value = sudokuPlay(sudoku.value, number, selected.value);
    return sudoku.value;
  }
  function play(number: string) {
    if (!selectedCell.value || selectedCell.value.frozen) {
      return;
    }
    const n = parseInt(number);
    playNumber(n);
    history.commit();
    if (isSolved.value) {
      meta.win(elapsedTime.value);
      stop();
    }
  }
  function playCandidate(number: string) {
    const n = parseInt(number);
    if (!n) {
      return;
    }
    if (!includes(SUDOKU_NUMBERS, n)) {
      throw new Error(`Invalid sudoku value: ${number}`);
    }
    if (
      !selected.value ||
      !selectedCell.value ||
      selectedCell.value.frozen ||
      selectedCell.value.value
    ) {
      return;
    }
    sudoku.value = toggleCandidate(sudoku.value, n, selected.value);
    history.commit();
  }
  function toggleAnnotationEnabled() {
    annotationEnabled.value = !annotationEnabled.value;
  }
  function undo() {
    history.undo();
  }
  function redo() {
    history.redo();
  }

  return {
    sudoku,
    selected,
    startedAt,
    annotationEnabled,
    stopwatch,

    started,
    selectedCell,
    isSolved,
    hasUndo,
    hasRedo,
    numbersEnabled,
    selectedAreas,
    elapsedTime,
    valuesCompleted,
    currentNumber,

    start,
    startChallenge,
    stop,
    setSelected,
    move,
    playNumber,
    play,
    playCandidate,
    toggleAnnotationEnabled,
    undo,
    redo,
    restart,
    setCurrentNumber,
  };
});

