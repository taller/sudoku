import { defineStore } from "pinia";

const themes = {
    light: {
        'bg': '#ffffff',
        'border-color':'#000000',
        'selected-color': '#61b4eb',
        'selected-area-color': '#d4e7f4',
        'frozen-color': '#000000',
        'highlighted-background-color': '#9dd3f7',
        'wrong-text-color': '#cc5430',
        'wrong-background-color':'#f79490',
        'text-color':'#0484fc',
        'border-width': '1px',
        'border-width-wide': '5px',
        'controls-background-hover':'#e6e1e1',
        'controls-color-text': 'black',
        'controls-disabled-color': '#918787',
        'controls-selected-color': '#9dd3f7',
        'controls-border-color': 'rgb(13, 199, 38)',
        'controls-bg-color': '#d9f3db',
        'controls-completed-color': '#d9f3db' ,
        'menu-bg-history': 'rgb(222, 238, 248)',
    },
    dark: {
        'bg': '#242424',
        'border-color':'#4f5458',
        'selected-color': '#4a728a',
        'selected-area-color': '#2e3d47',
        'frozen-color': '#969899',
        'highlighted-background-color': '#3b5d70',
        'wrong-text-color': '#cc5430',
        'wrong-background-color':'#4d2e25',
        'text-color':'#2295e7',
        'border-width': '1px',
        'border-width-wide': '5px',
        'controls-background-hover':'#444',
        'controls-color-text': 'white',
        'controls-disabled-color': '#444',
        'controls-selected-color': '#918787',
        'controls-border-color': 'yellowgreen',
        'controls-bg-color': '#749d77',
        'controls-completed-color': 'green',
        'menu-bg-history':'rgb(64, 64, 65)',
    }
};

export type ThemeName = keyof typeof themes

export type ThemeState = {
    currentTheme: ThemeName;
}

export const useThemeStore = defineStore('theme', {
    state: (): ThemeState => {
        return {
            currentTheme: 'light'
        }
    },
    getters: {
        themes(): ThemeName[] {
            return Object.keys(themes) as ThemeName[];
        },
        t(state){
            return themes[state.currentTheme];
        }
    },
    actions: {
        setTheme(themeName: ThemeName){
            this.currentTheme = themeName;
        }
    },
  })