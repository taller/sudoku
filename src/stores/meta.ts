import { defineStore } from "pinia";
import { ref, computed } from "vue";
import { Sudoku, SudokuGame, SudokuGameFinished, toArray } from "../sudoku";
import { useStorage, useUrlSearchParams } from "@vueuse/core";

export const useMetaStore = defineStore("meta", () => {
    const current = ref<SudokuGame | null>(null);
    const won = ref<SudokuGameFinished | null>(null);
    const history = useStorage<SudokuGameFinished[]>('history', []);
    const last10 = computed (() => {
        const last = history.value.slice(-10)
        return last.reverse()
    })

    const searchParams = useUrlSearchParams<{s:string, t:string, d:string}>()
    const challenge = ref<SudokuGameFinished | null>(null);
    if (searchParams.s && searchParams.t){
        challenge.value = {
            puzzle: searchParams.s.split('').map(x => parseInt(x)),
            difficulty: parseFloat(searchParams.d) || 0,
            time: searchParams.t,
        }       
    }
    
    function start(sudoku: Sudoku, difficulty: number) {
        won.value = null;
        current.value = {
            puzzle: toArray(sudoku),
            difficulty
        };
    }

    function stop(){
        current.value = null;
        challenge.value = null;
    }

    function win(time: string) {
        if (!current.value) throw new Error('cannot win if there is no current game')
        won.value = {
            ...current.value,
            time,
        };
        history.value.push(won.value);
    }

    return {
        current,
        won,
        history,
        last10,
        challenge,

        start,
        win,
        stop
    }
});
