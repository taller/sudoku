/**
 *  Como Array.includes no va bien en el caso que quería probar `SUDOKU_COORDS.includes(number)`
 *  hizo falta crear esta función includes que incluye type narrowing
 * @param coll 
 * @param el 
 * @returns 
 * @link https://fettblog.eu/typescript-array-includes/
 */
export function includes<T extends U, U>(coll: ReadonlyArray<T>, el: U): el is T {
    return coll.includes(el as T);
}